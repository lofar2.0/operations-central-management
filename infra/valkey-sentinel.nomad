job "valkey-sentinel" {
  datacenters = ["nl-north"]
  type = "service"
  namespace = "operations"

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    auto_revert = false
    canary = 0
  }

  group "sentinel" {
    count = 3


    network {
      mode = "host"
      port "sentinel" {
        to           = 26379
      }
    }

    task "valkey" {
      driver = "docker"

      config {
        image = "valkey/valkey:7"
        ports = ["sentinel"]
        args    = ["/local/sentinel.conf", "--sentinel"]

      }

      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }
      template {
        destination   = "local/sentinel.conf"
        change_mode = "restart"
        perms = "666"
        data          = <<EOF
port 26379
sentinel announce-ip {{ env "NOMAD_IP_sentinel" }}
sentinel announce-port  {{ env "NOMAD_HOST_PORT_sentinel" }}

sentinel resolve-hostnames yes
EOF
      }

      service {
        name = "operations-valkey-sentinel"
        port = "sentinel"
        check {
          name     = "alive"
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }

}
