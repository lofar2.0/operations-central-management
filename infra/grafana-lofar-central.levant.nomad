job "grafana" {
  datacenters = ["nl-north"]
  type = "service"
  namespace = "operations"

  constraint {
    attribute = "${attr.kernel.name}"
    value = "linux"
  }

  update {
    stagger = "30s"
    max_parallel = 1
  }
    reschedule {
      unlimited = true
      delay = "30s"
      delay_function = "constant"
    }

  group "grafana" {
    restart {
      attempts = 10
      interval = "5m"
      delay    = "10s"
      mode     = "delay"
    }

    network {
      mode = "bridge"
      port "https" {
        to     = 443
      }

      port "http_health" {
        to     = 8008
      }
    }

    vault {
      policies = ["default"]
    }

    service {
      tags = ["scrape", "cert", "lb"]
      name = "monitoring"
      port = "https"
      task = "nginx"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "metrics"
              local_bind_port  = 9009
            }
            upstreams {
              destination_name = "operations-grafana-postgres"
              local_bind_port  = 5432
            }
            upstreams {
              destination_name = "operations-oncall"
              local_bind_port  = 8080
            }
          }
        }
      }
    }

    task "grafana" {
      driver = "docker"
      vault {
        change_mode = "noop"
        policies = ["default"]
      }
      config {
        image = "[[.registry.astron.url]]/lofar2.0/operations-central-management/grafana:[[.image_tag]]"
        mount {
          type   = "bind"
          source = "local/datasources/"
          target = "/etc/grafana/provisioning/datasources/"
        }
        extra_hosts = [
          "prometheus:127.0.0.1"
        ]
      }

      env {
        GF_SERVER_DOMAIN              = "monitoring.lofar.net"

        GF_DATABASE_TYPE = "postgres"
        GF_DATABASE_HOST = "localhost:5432"
        GF_DATABASE_NAME = "grafana"
        GF_DATABASE_USER = "postgres"
        GF_DATABASE_PASSWORD = "password"

      }

      template {
        env = true
        destination = "secrets/file.env"
        data = <<EOH
        SLACK_TOKEN = "{{with secret "kv-v2/data/grafana/slack"}}{{.Data.data.token}}{{end}}"
        EOH
      }

      template {
        data = <<EOH
apiVersion: 1

datasources:
  # <string, required> name of the datasource. Required
  - name: Prometheus
    # <string, required> datasource type. Required
    type: prometheus
    # <string, required> access mode. proxy or direct (Server or Browser in the UI). Required
    access: proxy
    # <int> org id. will default to orgId 1 if not specified
    orgId: 1
    # <string> custom UID which can be used to reference this datasource in other parts of the configuration, if not specified will be generated automatically
    uid: prometheus
    # <string> url
    url: http://metrics.service.consul:9009/prometheus
    # <string> Deprecated, use secureJsonData.password
    password:
    # <string> database user, if used
    user:
    # <string> database name, if used
    database:
    # <bool> enable/disable basic auth
    basicAuth: false
    # <string> basic auth username
    basicAuthUser:
    # <string> Deprecated, use secureJsonData.basicAuthPassword
    basicAuthPassword:
    # <bool> enable/disable with credentials headers
    withCredentials:
    # <bool> mark as default datasource. Max one per org
    isDefault: true
    # <map> fields that will be converted to json and stored in jsonData
    jsonData:
      httpMethod: POST
      httpHeaderName1: 'X-Scope-OrgID'
    # <string> json object of data that will be encrypted.
    secureJsonData:
      httpHeaderValue1: 'lofar'
    version: 1
    # <bool> allow users to edit datasources from the UI.
    editable: false
        EOH
        destination = "local/datasources/prometheus.yaml"
      }
      // template {
      //   data = <<EOH
      //   [auth.generic_oauth]
      //   name = ASTRON Keycloak
      //   allow_sign_up = true
      //   auto_login = false
      //   scopes = email profile roles
      //   auth_url = https://keycloak.astron.nl/auth/realms/SDC/protocol/openid-connect/auth
      //   token_url = https://keycloak.astron.nl/auth/realms/SDC/protocol/openid-connect/token
      //   api_url = https://keycloak.astron.nl/auth/realms/SDC/protocol/openid-connect/userinfo
      //   EOH

      //   destination = "local/grafana.ini"
      // }

      resources {
        cpu    = 2500
        memory = 2048
      }
    }

    task "nginx" {
      driver = "docker"
      config {
        image = "nginx:alpine"
        ports = ["https", "http_health"]
        mount {
          type   = "bind"
          target = "/etc/nginx/conf.d/default.conf"
          source = "local/nginx.conf"
          #readonly = false
        }
      }
      env {
        NGINX_HOST    = "monitoring.lofar.net"
      }

      service {
        port = "http_health"

        check {
            type            = "http"
          path            = "/api/health"
          interval        = "10s"
          timeout         = "1s"
        }
      }

      template {
        data = <<EOH
          map $http_upgrade $connection_upgrade {
            default upgrade;
            '' close;
          }
          upstream grafana {
            server localhost:3000;
          }
          server {
            listen 443 ssl proxy_protocol;
            listen 8008;
            server_name monitoring.lofar.net;
            server_name_in_redirect on;
            ssl_certificate {{ env "NOMAD_SECRETS_DIR" }}/fullchain.cer;
            ssl_certificate_key {{ env "NOMAD_SECRETS_DIR" }}/key.key;
            ssl_session_timeout  10m;
            ssl_protocols  TLSv1.2 TLSv1.3;
            ssl_ciphers    HIGH:!aNULL:!MD5;
            client_body_buffer_size     10M;
            client_max_body_size        10M;
            #add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
            if ($host != $server_name) {
              return 301 https://$server_name$request_uri;
            }
            location / {
              proxy_set_header Host $host;
              proxy_pass http://grafana;
            }
            # Proxy Grafana Live WebSocket connections.
            location /api/live/ {
              proxy_http_version 1.1;
              proxy_set_header Upgrade $http_upgrade;
              proxy_set_header Connection $connection_upgrade;
              proxy_set_header Host $host;
              proxy_pass http://grafana;
            }
          }
        EOH
        destination = "local/nginx.conf"
      }
      template {
        data = <<EOH
{{with secret "lets-encrypt/certificates/monitoring.lofar.net" -}}
{{.Data.data.cert }}
{{.Data.data.chain -}}
{{end}}
EOH
        destination = "${NOMAD_SECRETS_DIR}/fullchain.cer"
      }
      template {
        data = <<EOH
{{with secret "lets-encrypt/certificates/monitoring.lofar.net" -}}
{{.Data.data.key -}}
{{end}}
EOH
        destination = "${NOMAD_SECRETS_DIR}/key.key"
      }
    }
  }

  group "postgres" {
    count = 1

    volume "operations" {
      type            = "csi"
      source          = "operations"
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }


    network {
      mode = "bridge"
      port "postgres" {
        to = 5432
      }
    }

    service {
      name = "operations-grafana-postgres"
      port = "postgres"
      task = "postgres"
      address_mode = "alloc"

      connect {
        sidecar_service {}
      }
    }

    task "postgres" {
      driver = "docker"
      config {
        image = "postgres"
        ports = ["postgres"]
      }
      volume_mount {
        volume      = "operations"
        destination = "/var/lib"
      }

      env {
        POSTGRES_DB="grafana"
        POSTGRES_USER="postgres"
        POSTGRES_PASSWORD="password"
      }

      resources {
        cpu    = 2500
        memory = 1024
      }
    }
  }

  group "oncall" {
    restart {
      attempts = 10
      interval = "5m"
      delay    = "10s"
      mode     = "delay"
    }

    network {
      mode = "bridge"
      port "oncall" {
        static = 8080
      }
    }

    service {
      name = "operations-oncall"
      port = "oncall"
      task = "oncall"

      check {
        type     = "http"
        path     = "/"
        interval = "10s"
        timeout  = "1s"
      }

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "operations-grafana-postgres"
              local_bind_port  = 5432
            }
          }
        }
      }
    }

    task "oncall" {
      driver = "docker"

      config {
        image = "grafana/oncall:latest"
        ports = ["oncall"]
        command = "bash"
        args  = [ "-c", "python manage.py migrate --noinput && uwsgi --ini uwsgi.ini" ]
      }

      env {
        BROKER_TYPE            = "redis"
        REDIS_URI              = "redis://operations-valkey.service.consul:26379/1"
        DJANGO_SETTINGS_MODULE = "settings.hobby"
        GRAFANA_API_URL        = "https://monitoring.lofar.net"
        SECRET_KEY             = "my-little-secret-key-passphrase-thingy"
        BASE_URL               = "http://127.0.0.1:8080"

        DATABASE_TYPE          = "postgresql"
        DATABASE_NAME          = "grafana"
        DATABASE_USER          = "postgres"
        DATABASE_PASSWORD      = "password"
        DATABASE_HOST          = "127.0.0.1"
        DATABASE_PORT          = "5432"
      }

      resources {
        cpu    = 2500
        memory = 1024
      }
    }

    task "celery" {
      driver = "docker"

      config {
        image = "grafana/oncall:latest"
        args  = [
          "sh",
          "-c",
          "./celery_with_exporter.sh"
        ]
      }

      env {
        BROKER_TYPE                       = "redis"
        REDIS_URI                         = "redis://operations-valkey.service.consul:26379/1"

        CELERY_WORKER_QUEUE               = "default,critical,long,slack,telegram,webhook,retry,celery,grafana"
        CELERY_WORKER_CONCURRENCY         = "1"
        CELERY_WORKER_MAX_TASKS_PER_CHILD = "100"
        CELERY_WORKER_SHUTDOWN_INTERVAL   = "65m"
        CELERY_WORKER_BEAT_ENABLED        = "True"

        DJANGO_SETTINGS_MODULE            = "settings.hobby"
        SECRET_KEY                        = "my-little-secret-key-passphrase-thingy"

        GRAFANA_API_URL        = "https://monitoring.lofar.net"

        DATABASE_TYPE          = "postgresql"
        DATABASE_NAME          = "grafana"
        DATABASE_USER          = "postgres"
        DATABASE_PASSWORD      = "password"
        DATABASE_HOST          = "127.0.0.1"
        DATABASE_PORT          = "5432"
      }

      resources {
        cpu    = 2500
        memory = 1024
      }
    }
  }
}


