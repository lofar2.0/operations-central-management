job "tracing" {
  datacenters = ["nl-north"]
  type = "service"
  namespace = "observability"

  update {
    stagger = "30s"
    max_parallel = 1
  }

  group "tempo" {
    network {
      mode ="bridge"
      port "http" {
        static = 3200
      }
    }

    service {
      name = "tracing"
      port = "http"
      connect {
        sidecar_service { }
      }
    }

    task "tempo" {
      driver = "docker"
      config {
        image   = "grafana/tempo:latest"
        ports   = ["http"]
        force_pull = true
        args    = ["-config.file=/etc/tempo/config.yaml"]

        mount {
          type   = "bind"
          source = "local/tempo/"
          target = "/etc/tempo/"
        }
      }

      template {
        data = <<EOH
target: all
multitenancy_enabled: true
stream_over_http_enabled: true
server:
  http_listen_port: 3200
  log_level: info

ingester:
  max_block_duration: 5m               # cut the headblock when this much time passes. this is being set for demo purposes and should probably be left alone normally

compactor:
  compaction:
    block_retention: 1h                # overall Tempo trace retention. set for demo purposes

metrics_generator:

storage:
  trace:
    backend: s3                     # backend configuration to use
    s3:
      bucket: central-tracing
      endpoint: s3.lofar.net
      access_key: minioadmin
      secret_key: minioadmin
      forcepathstyle: true
    wal:
      path: /tmp/tempo/wal             # where to store the the wal locally
    local:
      path: /tmp/tempo/blocks

overrides:
  defaults:
    metrics_generator:
        EOH

        destination = "local/tempo/config.yaml"
      }
      resources {
        cpu    = 2048
        memory = 8096
      }
    }

  }
}
