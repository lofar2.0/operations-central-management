job "valkey" {
  datacenters = ["nl-north"]
  type = "service"
  namespace = "operations"

  group "database" {
    count = 2

    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }
    update {
      max_parallel      = 1
      health_check      = "checks"
      min_healthy_time  = "10s"
      healthy_deadline  = "5m"
      progress_deadline = "10m"
      auto_revert       = true
      auto_promote      = true
      canary            = 1
      stagger           = "30s"
    }

    ephemeral_disk {
      migrate = true
      size    = 500
      sticky  = true
    }

    network {
      port "db" {
        static = "26379"
      }
    }

    task "sentinel-discovery" {
      driver = "docker"
      lifecycle {
        hook = "prestart"
        sidecar = false
      }

      config {
        image = "valkey/valkey:7"
        ports = ["db"]
        command  = "/bin/bash"
        args = ["/local/generate-config.sh"]
      }
      template {
        destination   = "local/generate-config.sh"
        change_mode   = "restart"
        data          = <<EOF
{{ range service "operations-valkey-sentinel" }}
SENTINEL_URL={{ .Address}}
SENTINEL_PORT={{ .Port }}
{{ end -}}
readarray -t MASTER <<< "$(valkey-cli --raw -h $SENTINEL_URL -p $SENTINEL_PORT sentinel get-master-addr-by-name master | head -2)"
touch {{ env "NOMAD_ALLOC_DIR" }}/valkey.conf
chmod 0666 {{ env "NOMAD_ALLOC_DIR" }}/valkey.conf
if [[ -z "${MASTER[0]}" ]]
then
  echo "first, initialize sentinel"
{{ range service "operations-valkey-sentinel" }}
  valkey-cli --raw -h {{ .Address}} -p {{ .Port }} sentinel monitor master {{ env "NOMAD_IP_db" }} {{ env "NOMAD_PORT_db" }} 1
  valkey-cli --raw -h {{ .Address}} -p {{ .Port }} <<FOE
sentinel set master down-after-milliseconds 1000
sentinel set master parallel-syncs 1
sentinel set master failover-timeout 1800
FOE
{{ end -}}
# elif [[ "${MASTER[0]}" == "{{ env "NOMAD_IP_db" }}" ]]; then
#   echo "master, staying leader"
else
  echo "add master ${MASTER[0]}:${MASTER[1]} to redis.conf"
  echo "slaveof ${MASTER[0]} ${MASTER[1]}" >> {{ env "NOMAD_ALLOC_DIR" }}/valkey.conf
  echo "replica-read-only no" >> {{ env "NOMAD_ALLOC_DIR" }}/valkey.conf
fi
EOF
      }
    }

    task "valkey" {
      driver = "docker"

      config {
        image = "valkey/valkey:7"
        ports = ["db"]
        args = [
          "${NOMAD_ALLOC_DIR}/valkey.conf",
          "--port", "${NOMAD_PORT_db}"
        ]
      }

      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }

      service {
        name = "operations-valkey"
        port = "db"
        check {
          name     = "alive"
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }

      }
    }
  }

}
