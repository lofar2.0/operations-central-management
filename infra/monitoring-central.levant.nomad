job "monitoring" {
  datacenters = ["nl-north"]
  type        = "service"
  namespace   = "observability"

  # blackbox monitors http/https/tcp endpoints
  group "blackbox" {
    count = 1

    network {
      port "http" {
        to     = 9115
        static = 9115
      }
    }

    service {
      tags = ["scrape"]
      name = "blackbox"
      task = "blackbox"
      port = "http"
    }

    task "blackbox-exporter" {
      driver = "docker"

      config {
        image   = "quay.io/prometheus/blackbox-exporter:latest"
        ports   = ["http"]
      }
      resources {
        cpu    = 100
        memory = 100
      }
    }
  }
}
