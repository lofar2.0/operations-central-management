job "logs" {
  datacenters = ["nl-north"]
  type = "service"
  namespace = "observability"

  update {
    stagger = "30s"
    max_parallel = 1
  }

  group "loki" {
    network {
      mode ="bridge"
      port "http" {
        static = 3100
      }
    }

    service {
      tags = ["scrape"]
      name = "logs"
      port = "http"
      connect {
        sidecar_service { }
      }
    }

    task "loki" {
      driver = "docker"
      config {
        image   = "grafana/loki:latest"
        ports   = ["http"]
        force_pull = true
        args    = ["-config.file=/etc/loki/config.yaml"]

        mount {
          type   = "bind"
          source = "local/loki/"
          target = "/etc/loki/"
        }
      }

      template {
        data = <<EOH

        auth_enabled: false

        common:
          ring:
            instance_addr: 127.0.0.1
            kvstore:
              store: memberlist
          replication_factor: 1
          path_prefix: /loki # Update this accordingly, data will be stored here.
          storage:
            s3:
              endpoint: s3.lofar.net
              access_key_id: minioadmin
              secret_access_key: minioadmin
              bucketnames: central-logs
              s3forcepathstyle: true
              region: NL

        limits_config:
          allow_structured_metadata: false
          ingestion_rate_mb: 100
          ingestion_burst_size_mb: 1000

        storage_config:
          filesystem:
            directory: /loki/chunks

        schema_config:
          configs:
            - from: "2024-01-01"
              index:
                period: 24h
                prefix: index_
              object_store: s3
              schema: v12
              store: tsdb
            - from: "2025-01-11"
              index:
                period: 24h
                prefix: index_
              object_store: s3
              schema: v13
              store: tsdb

        compactor:
          working_directory: /loki/compactor
          compaction_interval: 5m

        ruler:
          storage:
            type: s3
        server:
          log_level: warn
        EOH

        destination = "local/loki/config.yaml"
      }
      resources {
        cpu    = 2048
        memory = 8096
      }
    }

  }
}
