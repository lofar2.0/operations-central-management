To deploy manually, run:

docker run --rm -i --net=host hashicorp/levant deploy --var image_tag=MY_IMAGE_TAG /dev/stdin < file.levant.nomad
