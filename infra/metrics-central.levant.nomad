job "metrics" {
  datacenters = ["nl-north"]
  type = "service"
  namespace = "observability"

  update {
    stagger = "30s"
    max_parallel = 1
  }

  group "prometheus" {
    network {
      mode = "bridge"
    }

    service {
      tags         = ["scrape"]
      name         = "prometheus"
      port         = "9090"
      address_mode = "alloc"

      check {
        type     = "http"
        name     = "prometheus_health"
        port     = "9090"
        path     = "/-/healthy"
        interval = "20s"
        timeout  = "30s"
        address_mode = "alloc"
      }
    }

    task "prometheus" {
      driver = "docker"

      config {
        image = "[[.registry.astron.url]]/lofar2.0/tango/prometheus:[[.monitoring.prometheus.version]]"
        args  = [
          "--config.file=/local/prometheus.yaml",
          "--web.enable-remote-write-receiver",
          "--storage.tsdb.retention.time=7d"
        ]
      }

      template {
        data          = <<EOH
        global:
          evaluation_interval: 10s
          scrape_interval: 10s
          scrape_timeout: 10s

        remote_write:
          - url: http://metrics.service.lofar-central.consul:9009/api/v1/push
            headers:
              "X-Scope-OrgID": lofar
            write_relabel_configs:
              - target_label: host
                replacement: lofar-central

        scrape_configs:
          - job_name: 'tmss'
            static_configs:
              # scrape each django worker
              [[ range $port := loop 9001 9061 ]]
              - targets: ['lcs129.control.lofar:[[ $port ]]']
              [[ end ]]
          - job_name: 'qa_statistics_service'
            static_configs:
              - targets: ['lcs129.control.lofar:8002']
          - job_name: 'scheduling_service'
            static_configs:
              - targets: ['lcs129.control.lofar:8003']
          - job_name: 'lta_ingest_transfer_service'
            static_configs:
              - targets: ['lexar003.control.lofar:8001']
          - job_name: 'blackbox'
            params:
              module:
              - http_2xx
            metrics_path: '/probe'
            static_configs:
            - targets:
              - vault.lofar.net
              - s3.lofar.net
              - tmss.lofar.eu
              - lta.lofar.eu
              - git.astron.nl
            {{range services}}{{if in .Tags "cert"}}{{ if .Name | regexMatch "(.+)-sidecar-proxy$" }}{{ else }}
              - '{{.Name}}.lofar.net'
            {{end}}{{end}}{{end}}
            relabel_configs:
              - source_labels: [__address__]
                target_label: __param_target
              - source_labels: [__param_target]
                target_label: instance
              - target_label: __address__
                replacement: 'blackbox.service.consul:9115'
          - job_name: 'consul'
            metrics_path: '/v1/agent/metrics'
            params:
              format: ['prometheus']
            static_configs:
              - targets: ['consul.service.consul:8500']
          - job_name: 'nomad'
            metrics_path: '/v1/metrics'
            params:
              format: ['prometheus']
            consul_sd_configs:
            - server: 'consul.service.consul:8500'
              services: ['nomad']
            relabel_configs:
            - source_labels: ['__meta_consul_tags']
              regex: '(.*)http(.*)'
              action: keep
            - source_labels: [__meta_consul_service]
              target_label: instance # avoid a dynamic ip:port or hostname
          - job_name: 'consul_services'
            consul_sd_configs:
              - server: 'consul.service.consul:8500'
                services:
          {{range services}}{{if in .Tags "scrape"}}{{ if .Name | regexMatch "(.+)-sidecar-proxy$" }}{{ else }}
                  - '{{.Name}}'
          {{end}}{{end}}{{end}}
            relabel_configs:
              - source_labels: [__meta_consul_service_metadata_metrics_path]
                regex:  '(/.*)'            # capture '/...' part
                target_label: __metrics_path__  # change metrics path
              - source_labels: [__meta_consul_service_metadata_metrics_address]
                regex:  '(.+)'
                target_label: __address__  # change address
              - source_labels: [__meta_consul_service]
                target_label: instance # avoid a dynamic ip:port or hostname
        EOH
        change_mode   = "signal"
        change_signal = "SIGHUP"
        destination   = "local/prometheus.yaml"
      }

      resources {
        cpu        = 500
        memory     = 2048
        memory_max = 8192
      }
    }
  }

  group "mimir" {
    network {
      mode = "bridge"
      port "http" {
        static = 9009
        to = 8080
      }
    }

    service {
      name = "metrics"
      port = "http"
      connect {
        sidecar_service { }
      }
    }

    task "mimir" {
      driver = "docker"
      config {
        image   = "grafana/mimir:latest"
        force_pull = true
        args    = ["-config.file=/etc/mimir/config.yaml"]

        mount {
          type   = "bind"
          source = "local/mimir/"
          target = "/etc/mimir/"
        }
      }

      template {
        data = <<EOH
        # Do not use this configuration in production.
        # It is for demonstration purposes only.
        # Run Mimir in single process mode, with all components running in 1 process.
        target: all,overrides-exporter,query-frontend

        multitenancy_enabled: true

        # Configure Mimir to use Minio as object storage backend.
        common:
          storage:
            backend: s3
            s3:
              endpoint: s3.lofar.net
              access_key_id: minioadmin
              secret_access_key: minioadmin
              bucket_name: central-metrics

        # Blocks storage requires a prefix when using a common object storage bucket.
        blocks_storage:
          storage_prefix: blocks
          tsdb:
            dir: /data/ingester

        # Use memberlist, a gossip-based protocol, to enable the 3 Mimir replicas to communicate
        #memberlist:
        #  join_members: [ mimir-1, mimir-2, mimir-3 ]

        # ruler:
        #  rule_path: /data/ruler
        #  alertmanager_url: http://127.0.0.1:8080/alertmanager
        #  ring:
        #    # Quickly detect unhealthy rulers to speed up the tutorial.
        #    heartbeat_period: 2s
        #    heartbeat_timeout: 10s

        api:
          skip_label_name_validation_header_enabled: true

        limits:
          max_label_value_length: 100000
          max_label_name_length: 100000
          ingestion_rate: 100000
          ingestion_burst_size: 2000000
          max_global_series_per_user: 20000000

        tenant_federation:
          enabled: true

        ingester:
          ring:
            replication_factor: 1

          instance_limits:
            max_ingestion_rate: 200000
            max_series: 15000000
            max_tenants: 10000
            max_inflight_push_requests: 300000

        distributor:
          instance_limits:
            max_ingestion_rate: 750000
            max_inflight_push_requests: 15000
            max_inflight_push_requests_bytes: 3145728000

        #alertmanager:
        #  data_dir: /data/alertmanager
        #  fallback_config_file: /etc/alertmanager-fallback-config.yaml
        #  external_url: http://localhost:9009/alertmanager

        server:
          log_level: warn
        EOH

        destination = "local/mimir/config.yaml"
      }
      resources {
        cpu    = 2048
        memory = 8096
      }
    }

  }
}
