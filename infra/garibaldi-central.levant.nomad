job "daily-backup-grafana-to-s3" {
  datacenters = ["nl-north"]
  type = "batch"
  namespace = "operations"

  periodic {
    crons = ["@daily"]
  }

  group "garibaldi" {
    vault {
      policies = ["default"]
    }

    task "garibaldi" {
      driver = "docker"
      config {
        image = "[[.registry.astron.url]]/grafana/garibaldi:latest"
      }
      env {
        MINIO_HOSTNAME = "https://s3.lofar.net:443"
        MINIO_ACCESS_KEY = "minioadmin"
        MINIO_SECRET_KEY = "minioadmin"
        MINIO_BUCKET = "grafana-backup"
        GRAFANA_URL = "https://monitoring.lofar.net:443"
      }

      # Note: the Grafana service account token needs to be recreated if Grafana is redeployed:
      #   1. Create an Admin service account at https://monitoring.lofar.net/org/serviceaccounts/
      #   2. Create a service account token for that account
      #   3. Upload the token to vault at https://vault.lofar.net/ui/vault/secrets/kv-v2/kv/grafana%2Fgaribaldi

      template {
        env = true
        destination = "secrets/file.env"
        data = <<EOH
        GRAFANA_TOKEN = "{{with secret "kv-v2/data/grafana/garibaldi"}}{{.Data.data.token}}{{end}}"
        EOH
      }

      resources {
        cpu = 2500
        memory = 512
      }
    }
  }
}
