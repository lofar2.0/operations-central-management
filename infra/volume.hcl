type = "csi"
id = "operations"
name = "operations"
namespace    = "operations"

capability {
  access_mode = "multi-node-multi-writer"
  attachment_mode = "file-system"
}
plugin_id = "juicefs0"

secrets {
  name="operations"
  metaurl="redis://operations-valkey.service.consul:26379/0"
  bucket="https://s3.lofar.net/csi-volumes/operations"
  storage="minio"
  access-key="minioadmin"
  secret-key="minioadmin"
}
