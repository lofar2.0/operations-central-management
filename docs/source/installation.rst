Installation
==================

You will need the following dependencies installed:

- docker
- docker-compose
- git
- make

You start with checking out the source code, f.e. the main branch, as well as the git submodules we use::

  git clone https://git.astron.nl/lofar2.0/operations-central-management.git
  cd operations-central-management
  git submodule init
  git submodule update

Next, we build our software as docker images. This may take a while::

  make build

To start the software, just start the containers::

  make start

and make sure they are all up and running::

  make status

You should see all containers in the ``Up`` state. If not, you can inspect why with ``docker logs <container>``. Note that the containers will automatically be restarted on failure, and also if you reboot. Stop them explicitly to bring them down (``make stop <container>``).
