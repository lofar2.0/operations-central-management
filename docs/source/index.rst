.. LOFAR2.0 Operations Central Management documentation master file, created by
   sphinx-quickstart on Mon May 16 13:39:08 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LOFAR2.0 Operations Central Management's documentation!
==================================================================

The Operations Central Management module is setup to monitor the LOFAR telescope, most specifically from the LOFAR 2.0 stations. It:

* Aggregates monitoring points across stations and other end points,
* Visualises the monitoring points,
* Generates and manages alerts defined on the monitoring points.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   intro
   user_manual
   dev_manual



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
