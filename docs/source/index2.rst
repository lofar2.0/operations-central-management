.. LOFAR2.0 Station Control documentation master file, created by
   sphinx-quickstart on Wed Oct  6 13:31:53 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LOFAR2.0 Operational Central Management's documentation!
=========================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
